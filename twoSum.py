class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        targetSet = set(nums)
        for num in range(len(nums)):
            if((target-nums[num]) in targetSet):
                if(target-nums[num] == nums[num]):
                    first = nums.index(nums[num])
                    if(first != num):
                        return [num,nums.index(target-nums[num])]
                    elif(nums[num] in nums[num+1:]):
                        return [num, nums.index(nums[num],num+1)]
                else:
                    return [num,nums.index(target-nums[num])]